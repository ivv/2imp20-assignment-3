<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:f7dac546-a72e-4a7e-b920-8134df74dbd3(FacilityProduct.sandbox)">
  <persistence version="9" />
  <languages>
    <use id="eb75abe2-b726-4c7b-b5c7-986053970fe8" name="FacilityProduct" version="0" />
  </languages>
  <imports />
  <registry>
    <language id="eb75abe2-b726-4c7b-b5c7-986053970fe8" name="FacilityProduct">
      <concept id="8971793897273538242" name="FacilityProduct.structure.Grid" flags="ng" index="2DwS71">
        <child id="8971793897273565938" name="nodes" index="2Dw1nL" />
        <child id="8971793897273565936" name="edges" index="2Dw1nN" />
      </concept>
      <concept id="8971793897273538250" name="FacilityProduct.structure.Edge" flags="ng" index="2DwS79">
        <reference id="4542203898784393239" name="node1" index="1zugTQ" />
        <reference id="4542203898784393241" name="node2" index="1zugTS" />
      </concept>
      <concept id="8971793897273538256" name="FacilityProduct.structure.Node" flags="ng" index="2DwS7j">
        <property id="7190384349627425329" name="y" index="1tQO99" />
        <property id="7190384349627425326" name="x" index="1tQO9m" />
        <property id="4542203898784392147" name="section" index="1zugmM" />
        <child id="4542203898784400796" name="shelfs" index="1zumfX" />
      </concept>
      <concept id="4542203898784398645" name="FacilityProduct.structure.Shelf" flags="ng" index="1zuhHk">
        <child id="4542203898784400794" name="products" index="1zumfV" />
      </concept>
      <concept id="4542203898784398648" name="FacilityProduct.structure.Product" flags="ng" index="1zuhHp" />
      <concept id="4542203898784387200" name="FacilityProduct.structure.Facility" flags="ng" index="1zujrx">
        <child id="4542203898784387201" name="grid" index="1zujrw" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1zujrx" id="3W99A5ss2eE">
    <property role="TrG5h" value="myFacility" />
    <node concept="2DwS71" id="3W99A5ss2eF" role="1zujrw">
      <node concept="2DwS7j" id="3W99A5ss3OK" role="2Dw1nL">
        <property role="TrG5h" value="test" />
        <property role="1tQO9m" value="1" />
        <property role="1tQO99" value="1" />
        <node concept="1zuhHk" id="3W99A5ss4EH" role="1zumfX">
          <node concept="1zuhHp" id="3W99A5ss4EJ" role="1zumfV" />
        </node>
        <node concept="1zuhHk" id="4GdRdjezRCi" role="1zumfX" />
      </node>
      <node concept="2DwS7j" id="3W99A5ss3OM" role="2Dw1nL">
        <property role="TrG5h" value="hoi" />
        <property role="1zugmM" value="GroenteAfdeling" />
        <property role="1tQO9m" value="1" />
        <property role="1tQO99" value="0" />
        <node concept="1zuhHk" id="3W99A5ss4Ey" role="1zumfX">
          <node concept="1zuhHp" id="3W99A5ss4E$" role="1zumfV" />
          <node concept="1zuhHp" id="3W99A5ss4YP" role="1zumfV" />
        </node>
        <node concept="1zuhHk" id="3W99A5ss4YS" role="1zumfX" />
      </node>
      <node concept="2DwS7j" id="3W99A5sskbW" role="2Dw1nL">
        <property role="TrG5h" value="hallo" />
        <property role="1tQO9m" value="1" />
        <property role="1tQO99" value="2" />
      </node>
      <node concept="2DwS7j" id="4GdRdje$24a" role="2Dw1nL">
        <property role="TrG5h" value="Jur" />
        <property role="1tQO9m" value="1" />
        <property role="1tQO99" value="3" />
      </node>
      <node concept="2DwS7j" id="4GdRdje$UY9" role="2Dw1nL">
        <property role="TrG5h" value="Groente" />
        <property role="1zugmM" value="Groente" />
        <property role="1tQO9m" value="2" />
        <property role="1tQO99" value="2" />
        <node concept="1zuhHk" id="4GdRdje$UYm" role="1zumfX">
          <node concept="1zuhHp" id="4GdRdje$UYo" role="1zumfV" />
        </node>
        <node concept="1zuhHk" id="6f9o7wY4JUA" role="1zumfX">
          <node concept="1zuhHp" id="6f9o7wY4JUE" role="1zumfV" />
          <node concept="1zuhHp" id="6f9o7wY4JUG" role="1zumfV" />
        </node>
      </node>
      <node concept="2DwS79" id="3W99A5ss2eH" role="2Dw1nN">
        <ref role="1zugTQ" node="3W99A5ss3OM" resolve="hoi" />
        <ref role="1zugTS" node="3W99A5ss3OK" resolve="test" />
      </node>
      <node concept="2DwS79" id="3W99A5ss4EA" role="2Dw1nN">
        <ref role="1zugTQ" node="3W99A5ss3OK" resolve="test" />
        <ref role="1zugTS" node="3W99A5sskbW" resolve="hallo" />
      </node>
      <node concept="2DwS79" id="3W99A5ss2eM" role="2Dw1nN">
        <ref role="1zugTQ" node="3W99A5sskbW" resolve="hallo" />
        <ref role="1zugTS" node="4GdRdje$UY9" resolve="Groente" />
      </node>
      <node concept="2DwS79" id="3W99A5ss2eQ" role="2Dw1nN">
        <ref role="1zugTS" node="4GdRdje$24a" resolve="Jur" />
        <ref role="1zugTQ" node="3W99A5sskbW" resolve="hallo" />
      </node>
      <node concept="2DwS79" id="3W99A5ss2eV" role="2Dw1nN" />
    </node>
  </node>
</model>

