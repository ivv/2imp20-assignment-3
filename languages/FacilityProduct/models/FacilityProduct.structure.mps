<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:482d45f2-47b2-4909-b645-235519915cd5(FacilityProduct.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <use id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage" version="11" />
    <use id="0272d3b4-4cc8-481e-9e2f-07793fbfcb41" name="jetbrains.mps.lang.editor.table" version="0" />
    <devkit ref="78434eb8-b0e5-444b-850d-e7c4ad2da9ab(jetbrains.mps.devkit.aspect.structure)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="4fqr" ref="r:fa713d69-08ea-4732-b1f2-cb07f9e103ef(jetbrains.mps.execution.util.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="3348158742936976480" name="jetbrains.mps.lang.structure.structure.EnumerationMemberDeclaration" flags="ng" index="25R33">
        <property id="1421157252384165432" name="memberId" index="3tVfz5" />
      </concept>
      <concept id="3348158742936976479" name="jetbrains.mps.lang.structure.structure.EnumerationDeclaration" flags="ng" index="25R3W">
        <child id="3348158742936976577" name="members" index="25R1y" />
      </concept>
      <concept id="1082978164218" name="jetbrains.mps.lang.structure.structure.DataTypeDeclaration" flags="ng" index="AxPO6">
        <property id="7791109065626895363" name="datatypeId" index="3F6X1D" />
      </concept>
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="4628067390765907488" name="conceptShortDescription" index="R4oN_" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
        <child id="1071489727084" name="propertyDeclaration" index="1TKVEl" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288299" name="jetbrains.mps.lang.structure.structure.PropertyDeclaration" flags="ig" index="1TJgyi">
        <property id="241647608299431129" name="propertyId" index="IQ2nx" />
        <reference id="1082985295845" name="dataType" index="AX2Wp" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="7M2dKfl0rr2">
    <property role="EcuMT" value="8971793897273538242" />
    <property role="TrG5h" value="Grid" />
    <property role="34LRSv" value="grid" />
    <property role="R4oN_" value="facility layout" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="7M2dKfl0ybM" role="1TKVEi">
      <property role="IQ2ns" value="8971793897273565938" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="nodes" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="7M2dKfl0rrg" resolve="Node" />
    </node>
    <node concept="1TJgyj" id="7M2dKfl0ybK" role="1TKVEi">
      <property role="IQ2ns" value="8971793897273565936" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="edges" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="7M2dKfl0rra" resolve="Edge" />
    </node>
  </node>
  <node concept="1TIwiD" id="7M2dKfl0rra">
    <property role="EcuMT" value="8971793897273538250" />
    <property role="TrG5h" value="Edge" />
    <property role="34LRSv" value="edge" />
    <property role="R4oN_" value="Walkway" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3W99A5ss2wn" role="1TKVEi">
      <property role="IQ2ns" value="4542203898784393239" />
      <property role="20kJfa" value="node1" />
      <ref role="20lvS9" node="7M2dKfl0rrg" resolve="Node" />
    </node>
    <node concept="1TJgyj" id="3W99A5ss2wp" role="1TKVEi">
      <property role="IQ2ns" value="4542203898784393241" />
      <property role="20kJfa" value="node2" />
      <ref role="20lvS9" node="7M2dKfl0rrg" resolve="Node" />
    </node>
  </node>
  <node concept="1TIwiD" id="7M2dKfl0rrg">
    <property role="EcuMT" value="8971793897273538256" />
    <property role="TrG5h" value="Node" />
    <property role="34LRSv" value="node" />
    <property role="R4oN_" value="facility section" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyi" id="3W99A5ss2fj" role="1TKVEl">
      <property role="IQ2nx" value="4542203898784392147" />
      <property role="TrG5h" value="section" />
      <ref role="AX2Wp" to="tpck:fKAOsGN" resolve="string" />
    </node>
    <node concept="1TJgyi" id="6f9o7wY4y8I" role="1TKVEl">
      <property role="IQ2nx" value="7190384349627425326" />
      <property role="TrG5h" value="x" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="6f9o7wY4y8L" role="1TKVEl">
      <property role="IQ2nx" value="7190384349627425329" />
      <property role="TrG5h" value="y" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="PrWs8" id="3W99A5ss3_0" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="3W99A5ss4ms" role="1TKVEi">
      <property role="IQ2ns" value="4542203898784400796" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="shelfs" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3W99A5ss3OP" resolve="Shelf" />
    </node>
  </node>
  <node concept="1TIwiD" id="3W99A5ss120">
    <property role="EcuMT" value="4542203898784387200" />
    <property role="TrG5h" value="Facility" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="facility" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3W99A5ss121" role="1TKVEi">
      <property role="IQ2ns" value="4542203898784387201" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="grid" />
      <ref role="20lvS9" node="7M2dKfl0rr2" resolve="Grid" />
    </node>
    <node concept="PrWs8" id="3W99A5ss12j" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="36iPLFIb6rS" role="PzmwI">
      <ref role="PrY4T" to="4fqr:431DWIovi3l" resolve="IMainClass" />
    </node>
  </node>
  <node concept="1TIwiD" id="3W99A5ss3OP">
    <property role="EcuMT" value="4542203898784398645" />
    <property role="TrG5h" value="Shelf" />
    <property role="34LRSv" value="shelf" />
    <property role="R4oN_" value="contains products" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="1TJgyj" id="3W99A5ss4mq" role="1TKVEi">
      <property role="IQ2ns" value="4542203898784400794" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="products" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="3W99A5ss3OS" resolve="Product" />
    </node>
  </node>
  <node concept="1TIwiD" id="3W99A5ss3OS">
    <property role="EcuMT" value="4542203898784398648" />
    <property role="TrG5h" value="Product" />
    <property role="34LRSv" value="product" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" resolve="BaseConcept" />
    <node concept="PrWs8" id="3W99A5ss3OT" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyi" id="3W99A5ss3OV" role="1TKVEl">
      <property role="IQ2nx" value="4542203898784398651" />
      <property role="TrG5h" value="price" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3W99A5ss3OX" role="1TKVEl">
      <property role="IQ2nx" value="4542203898784398653" />
      <property role="TrG5h" value="quantity" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
    <node concept="1TJgyi" id="3W99A5ss3P0" role="1TKVEl">
      <property role="IQ2nx" value="4542203898784398656" />
      <property role="TrG5h" value="size" />
      <ref role="AX2Wp" to="tpck:fKAQMTA" resolve="integer" />
    </node>
  </node>
  <node concept="25R3W" id="4GdRdje$UXL">
    <property role="3F6X1D" value="5408221534098796401" />
    <property role="TrG5h" value="Direction" />
    <node concept="25R33" id="4GdRdje$UXM" role="25R1y">
      <property role="3tVfz5" value="5408221534098796402" />
      <property role="TrG5h" value="Left" />
    </node>
    <node concept="25R33" id="4GdRdje$UXN" role="25R1y">
      <property role="3tVfz5" value="5408221534098796403" />
      <property role="TrG5h" value="Right" />
    </node>
    <node concept="25R33" id="4GdRdje$UXQ" role="25R1y">
      <property role="3tVfz5" value="5408221534098796406" />
      <property role="TrG5h" value="Down" />
    </node>
    <node concept="25R33" id="4GdRdje$UXU" role="25R1y">
      <property role="3tVfz5" value="5408221534098796410" />
      <property role="TrG5h" value="Up" />
    </node>
  </node>
</model>

