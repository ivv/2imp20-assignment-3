<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:b26a72ce-62c5-44e7-aec9-80ebdec0fa84(FacilityProduct.typesystem)">
  <persistence version="9" />
  <languages>
    <use id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem" version="5" />
    <devkit ref="00000000-0000-4000-0000-1de82b3a4936(jetbrains.mps.devkit.aspect.typesystem)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
    <import index="ynv" ref="r:482d45f2-47b2-4909-b645-235519915cd5(FacilityProduct.structure)" implicit="true" />
    <import index="tdwz" ref="r:96384125-9fb8-4b05-b3d3-e72eaf6773bd(FacilityProduct.behavior)" implicit="true" />
  </imports>
  <registry>
    <language id="a247e09e-2435-45ba-b8d2-07e93feba96a" name="jetbrains.mps.baseLanguage.tuples">
      <concept id="1238852151516" name="jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleType" flags="in" index="1LlUBW">
        <child id="1238852204892" name="componentType" index="1Lm7xW" />
      </concept>
      <concept id="1238857743184" name="jetbrains.mps.baseLanguage.tuples.structure.IndexedTupleMemberAccessExpression" flags="nn" index="1LFfDK">
        <child id="1238857764950" name="tuple" index="1LFl5Q" />
        <child id="1238857834412" name="index" index="1LF_Uc" />
      </concept>
    </language>
    <language id="f3061a53-9226-4cc5-a443-f952ceaf5816" name="jetbrains.mps.baseLanguage">
      <concept id="1080223426719" name="jetbrains.mps.baseLanguage.structure.OrExpression" flags="nn" index="22lmx$" />
      <concept id="4836112446988635817" name="jetbrains.mps.baseLanguage.structure.UndefinedType" flags="in" index="2jxLKc" />
      <concept id="1197027756228" name="jetbrains.mps.baseLanguage.structure.DotExpression" flags="nn" index="2OqwBi">
        <child id="1197027771414" name="operand" index="2Oq$k0" />
        <child id="1197027833540" name="operation" index="2OqNvi" />
      </concept>
      <concept id="1070475926800" name="jetbrains.mps.baseLanguage.structure.StringLiteral" flags="nn" index="Xl_RD">
        <property id="1070475926801" name="value" index="Xl_RC" />
      </concept>
      <concept id="1070534370425" name="jetbrains.mps.baseLanguage.structure.IntegerType" flags="in" index="10Oyi0" />
      <concept id="1070534644030" name="jetbrains.mps.baseLanguage.structure.BooleanType" flags="in" index="10P_77" />
      <concept id="1068431474542" name="jetbrains.mps.baseLanguage.structure.VariableDeclaration" flags="ng" index="33uBYm">
        <child id="1068431790190" name="initializer" index="33vP2m" />
      </concept>
      <concept id="1068498886296" name="jetbrains.mps.baseLanguage.structure.VariableReference" flags="nn" index="37vLTw">
        <reference id="1068581517664" name="variableDeclaration" index="3cqZAo" />
      </concept>
      <concept id="1225271221393" name="jetbrains.mps.baseLanguage.structure.NPENotEqualsExpression" flags="nn" index="17QLQc" />
      <concept id="1225271283259" name="jetbrains.mps.baseLanguage.structure.NPEEqualsExpression" flags="nn" index="17R0WA" />
      <concept id="4972933694980447171" name="jetbrains.mps.baseLanguage.structure.BaseVariableDeclaration" flags="ng" index="19Szcq">
        <child id="5680397130376446158" name="type" index="1tU5fm" />
      </concept>
      <concept id="1068580123152" name="jetbrains.mps.baseLanguage.structure.EqualsExpression" flags="nn" index="3clFbC" />
      <concept id="1068580123155" name="jetbrains.mps.baseLanguage.structure.ExpressionStatement" flags="nn" index="3clFbF">
        <child id="1068580123156" name="expression" index="3clFbG" />
      </concept>
      <concept id="1068580123157" name="jetbrains.mps.baseLanguage.structure.Statement" flags="nn" index="3clFbH" />
      <concept id="1068580123159" name="jetbrains.mps.baseLanguage.structure.IfStatement" flags="nn" index="3clFbJ">
        <child id="1068580123160" name="condition" index="3clFbw" />
        <child id="1068580123161" name="ifTrue" index="3clFbx" />
      </concept>
      <concept id="1068580123136" name="jetbrains.mps.baseLanguage.structure.StatementList" flags="sn" stub="5293379017992965193" index="3clFbS">
        <child id="1068581517665" name="statement" index="3cqZAp" />
      </concept>
      <concept id="1068580320020" name="jetbrains.mps.baseLanguage.structure.IntegerConstant" flags="nn" index="3cmrfG">
        <property id="1068580320021" name="value" index="3cmrfH" />
      </concept>
      <concept id="1068581242875" name="jetbrains.mps.baseLanguage.structure.PlusExpression" flags="nn" index="3cpWs3" />
      <concept id="1068581242864" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclarationStatement" flags="nn" index="3cpWs8">
        <child id="1068581242865" name="localVariableDeclaration" index="3cpWs9" />
      </concept>
      <concept id="1068581242869" name="jetbrains.mps.baseLanguage.structure.MinusExpression" flags="nn" index="3cpWsd" />
      <concept id="1068581242863" name="jetbrains.mps.baseLanguage.structure.LocalVariableDeclaration" flags="nr" index="3cpWsn" />
      <concept id="1079359253375" name="jetbrains.mps.baseLanguage.structure.ParenthesizedExpression" flags="nn" index="1eOMI4">
        <child id="1079359253376" name="expression" index="1eOMHV" />
      </concept>
      <concept id="1081506762703" name="jetbrains.mps.baseLanguage.structure.GreaterThanExpression" flags="nn" index="3eOSWO" />
      <concept id="1081516740877" name="jetbrains.mps.baseLanguage.structure.NotExpression" flags="nn" index="3fqX7Q">
        <child id="1081516765348" name="expression" index="3fr31v" />
      </concept>
      <concept id="1204053956946" name="jetbrains.mps.baseLanguage.structure.IMethodCall" flags="ng" index="1ndlxa">
        <reference id="1068499141037" name="baseMethodDeclaration" index="37wK5l" />
      </concept>
      <concept id="1081773326031" name="jetbrains.mps.baseLanguage.structure.BinaryOperation" flags="nn" index="3uHJSO">
        <child id="1081773367579" name="rightExpression" index="3uHU7w" />
        <child id="1081773367580" name="leftExpression" index="3uHU7B" />
      </concept>
      <concept id="1080120340718" name="jetbrains.mps.baseLanguage.structure.AndExpression" flags="nn" index="1Wc70l" />
    </language>
    <language id="fd392034-7849-419d-9071-12563d152375" name="jetbrains.mps.baseLanguage.closures">
      <concept id="1199569711397" name="jetbrains.mps.baseLanguage.closures.structure.ClosureLiteral" flags="nn" index="1bVj0M">
        <child id="1199569906740" name="parameter" index="1bW2Oz" />
        <child id="1199569916463" name="body" index="1bW5cS" />
      </concept>
    </language>
    <language id="7a5dda62-9140-4668-ab76-d5ed1746f2b2" name="jetbrains.mps.lang.typesystem">
      <concept id="1175517767210" name="jetbrains.mps.lang.typesystem.structure.ReportErrorStatement" flags="nn" index="2MkqsV">
        <child id="1175517851849" name="errorString" index="2MkJ7o" />
      </concept>
      <concept id="1195213580585" name="jetbrains.mps.lang.typesystem.structure.AbstractCheckingRule" flags="ig" index="18hYwZ">
        <child id="1195213635060" name="body" index="18ibNy" />
      </concept>
      <concept id="1195214364922" name="jetbrains.mps.lang.typesystem.structure.NonTypesystemRule" flags="ig" index="18kY7G" />
      <concept id="3937244445246642777" name="jetbrains.mps.lang.typesystem.structure.AbstractReportStatement" flags="ng" index="1urrMJ">
        <child id="3937244445246642781" name="nodeToReport" index="1urrMF" />
      </concept>
      <concept id="1174642788531" name="jetbrains.mps.lang.typesystem.structure.ConceptReference" flags="ig" index="1YaCAy">
        <reference id="1174642800329" name="concept" index="1YaFvo" />
      </concept>
      <concept id="1174648085619" name="jetbrains.mps.lang.typesystem.structure.AbstractRule" flags="ng" index="1YuPPy">
        <child id="1174648101952" name="applicableNode" index="1YuTPh" />
      </concept>
      <concept id="1174650418652" name="jetbrains.mps.lang.typesystem.structure.ApplicableNodeReference" flags="nn" index="1YBJjd">
        <reference id="1174650432090" name="applicableNode" index="1YBMHb" />
      </concept>
    </language>
    <language id="7866978e-a0f0-4cc7-81bc-4d213d9375e1" name="jetbrains.mps.lang.smodel">
      <concept id="1177026924588" name="jetbrains.mps.lang.smodel.structure.RefConcept_Reference" flags="nn" index="chp4Y">
        <reference id="1177026940964" name="conceptDeclaration" index="cht4Q" />
      </concept>
      <concept id="1179409122411" name="jetbrains.mps.lang.smodel.structure.Node_ConceptMethodCall" flags="nn" index="2qgKlT" />
      <concept id="2396822768958367367" name="jetbrains.mps.lang.smodel.structure.AbstractTypeCastExpression" flags="nn" index="$5XWr">
        <child id="6733348108486823193" name="leftExpression" index="1m5AlR" />
        <child id="3906496115198199033" name="conceptArgument" index="3oSUPX" />
      </concept>
      <concept id="1139613262185" name="jetbrains.mps.lang.smodel.structure.Node_GetParentOperation" flags="nn" index="1mfA1w" />
      <concept id="1140137987495" name="jetbrains.mps.lang.smodel.structure.SNodeTypeCastExpression" flags="nn" index="1PxgMI" />
      <concept id="1138055754698" name="jetbrains.mps.lang.smodel.structure.SNodeType" flags="in" index="3Tqbb2">
        <reference id="1138405853777" name="concept" index="ehGHo" />
      </concept>
      <concept id="1138056022639" name="jetbrains.mps.lang.smodel.structure.SPropertyAccess" flags="nn" index="3TrcHB">
        <reference id="1138056395725" name="property" index="3TsBF5" />
      </concept>
      <concept id="1138056143562" name="jetbrains.mps.lang.smodel.structure.SLinkAccess" flags="nn" index="3TrEf2">
        <reference id="1138056516764" name="link" index="3Tt5mk" />
      </concept>
      <concept id="1138056282393" name="jetbrains.mps.lang.smodel.structure.SLinkListAccess" flags="nn" index="3Tsc0h">
        <reference id="1138056546658" name="link" index="3TtcxE" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
    <language id="83888646-71ce-4f1c-9c53-c54016f6ad4f" name="jetbrains.mps.baseLanguage.collections">
      <concept id="1204796164442" name="jetbrains.mps.baseLanguage.collections.structure.InternalSequenceOperation" flags="nn" index="23sCx2">
        <child id="1204796294226" name="closure" index="23t8la" />
      </concept>
      <concept id="1235566554328" name="jetbrains.mps.baseLanguage.collections.structure.AnyOperation" flags="nn" index="2HwmR7" />
      <concept id="1203518072036" name="jetbrains.mps.baseLanguage.collections.structure.SmartClosureParameterDeclaration" flags="ig" index="Rh6nW" />
      <concept id="1162935959151" name="jetbrains.mps.baseLanguage.collections.structure.GetSizeOperation" flags="nn" index="34oBXx" />
      <concept id="1202120902084" name="jetbrains.mps.baseLanguage.collections.structure.WhereOperation" flags="nn" index="3zZkjj" />
    </language>
  </registry>
  <node concept="18kY7G" id="3W99A5ssktU">
    <property role="TrG5h" value="check_duplicate_node_name" />
    <node concept="3clFbS" id="3W99A5ssktV" role="18ibNy">
      <node concept="3clFbJ" id="3W99A5ssxpD" role="3cqZAp">
        <node concept="3clFbS" id="3W99A5ssxpF" role="3clFbx">
          <node concept="2MkqsV" id="3W99A5ssynl" role="3cqZAp">
            <node concept="3cpWs3" id="3W99A5ssyDx" role="2MkJ7o">
              <node concept="2OqwBi" id="3W99A5ssyQ3" role="3uHU7w">
                <node concept="1YBJjd" id="3W99A5ssyDN" role="2Oq$k0">
                  <ref role="1YBMHb" node="3W99A5ssktX" resolve="node" />
                </node>
                <node concept="3TrcHB" id="3W99A5ssz5X" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
              <node concept="Xl_RD" id="3W99A5ssynx" role="3uHU7B">
                <property role="Xl_RC" value="Duplicate Node name: " />
              </node>
            </node>
            <node concept="1YBJjd" id="3W99A5sszdK" role="1urrMF">
              <ref role="1YBMHb" node="3W99A5ssktX" resolve="node" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="3W99A5ssn4_" role="3clFbw">
          <node concept="2OqwBi" id="3W99A5sslek" role="2Oq$k0">
            <node concept="1PxgMI" id="3W99A5ssl6_" role="2Oq$k0">
              <node concept="chp4Y" id="3W99A5ssl7e" role="3oSUPX">
                <ref role="cht4Q" to="ynv:7M2dKfl0rr2" resolve="Grid" />
              </node>
              <node concept="2OqwBi" id="3W99A5sskJc" role="1m5AlR">
                <node concept="1YBJjd" id="3W99A5sskBm" role="2Oq$k0">
                  <ref role="1YBMHb" node="3W99A5ssktX" resolve="node" />
                </node>
                <node concept="1mfA1w" id="3W99A5sskRC" role="2OqNvi" />
              </node>
            </node>
            <node concept="3Tsc0h" id="3W99A5sslmi" role="2OqNvi">
              <ref role="3TtcxE" to="ynv:7M2dKfl0ybM" resolve="nodes" />
            </node>
          </node>
          <node concept="2HwmR7" id="3W99A5ssvA6" role="2OqNvi">
            <node concept="1bVj0M" id="3W99A5ssvA8" role="23t8la">
              <node concept="3clFbS" id="3W99A5ssvA9" role="1bW5cS">
                <node concept="3clFbF" id="3W99A5ssvAa" role="3cqZAp">
                  <node concept="1Wc70l" id="3W99A5ssvAb" role="3clFbG">
                    <node concept="17QLQc" id="3W99A5ssvAc" role="3uHU7w">
                      <node concept="1YBJjd" id="3W99A5ssvAd" role="3uHU7w">
                        <ref role="1YBMHb" node="3W99A5ssktX" resolve="node" />
                      </node>
                      <node concept="37vLTw" id="3W99A5ssvAe" role="3uHU7B">
                        <ref role="3cqZAo" node="3W99A5ssvAm" resolve="it" />
                      </node>
                    </node>
                    <node concept="17R0WA" id="3W99A5ssvAf" role="3uHU7B">
                      <node concept="2OqwBi" id="3W99A5ssvAg" role="3uHU7B">
                        <node concept="37vLTw" id="3W99A5ssvAh" role="2Oq$k0">
                          <ref role="3cqZAo" node="3W99A5ssvAm" resolve="it" />
                        </node>
                        <node concept="3TrcHB" id="3W99A5ssvAi" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                      <node concept="2OqwBi" id="3W99A5ssvAj" role="3uHU7w">
                        <node concept="1YBJjd" id="3W99A5ssvAk" role="2Oq$k0">
                          <ref role="1YBMHb" node="3W99A5ssktX" resolve="node" />
                        </node>
                        <node concept="3TrcHB" id="3W99A5ssvAl" role="2OqNvi">
                          <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="3W99A5ssvAm" role="1bW2Oz">
                <property role="TrG5h" value="it" />
                <node concept="2jxLKc" id="3W99A5ssvAn" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3W99A5ssktX" role="1YuTPh">
      <property role="TrG5h" value="node" />
      <ref role="1YaFvo" to="ynv:7M2dKfl0rrg" resolve="Node" />
    </node>
  </node>
  <node concept="18kY7G" id="3W99A5ssDHe">
    <property role="TrG5h" value="connected_grid_nodes" />
    <node concept="3clFbS" id="3W99A5ssDHf" role="18ibNy">
      <node concept="3cpWs8" id="4GdRdje$m3m" role="3cqZAp">
        <node concept="3cpWsn" id="4GdRdje$m3p" role="3cpWs9">
          <property role="TrG5h" value="result" />
          <node concept="1LlUBW" id="4GdRdje$m3k" role="1tU5fm">
            <node concept="10P_77" id="4GdRdje$m4z" role="1Lm7xW" />
            <node concept="3Tqbb2" id="4GdRdje$m4P" role="1Lm7xW">
              <ref role="ehGHo" to="ynv:7M2dKfl0rrg" resolve="Node" />
            </node>
          </node>
          <node concept="2OqwBi" id="4GdRdje$meF" role="33vP2m">
            <node concept="1YBJjd" id="4GdRdje$m5y" role="2Oq$k0">
              <ref role="1YBMHb" node="3W99A5ssDHX" resolve="grid" />
            </node>
            <node concept="2qgKlT" id="4GdRdje$mvD" role="2OqNvi">
              <ref role="37wK5l" to="tdwz:3W99A5ssVcY" resolve="isConnected" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="3W99A5svcgJ" role="3cqZAp">
        <node concept="3clFbS" id="3W99A5svcgL" role="3clFbx">
          <node concept="2MkqsV" id="3W99A5svcCH" role="3cqZAp">
            <node concept="Xl_RD" id="3W99A5svcCM" role="2MkJ7o">
              <property role="Xl_RC" value="A Node in this grid is not connected to all other nodes: " />
            </node>
            <node concept="1YBJjd" id="3W99A5svcNR" role="1urrMF">
              <ref role="1YBMHb" node="3W99A5ssDHX" resolve="grid" />
            </node>
          </node>
          <node concept="2MkqsV" id="4GdRdje$mXa" role="3cqZAp">
            <node concept="Xl_RD" id="4GdRdje$mXs" role="2MkJ7o">
              <property role="Xl_RC" value="This node is not reachable" />
            </node>
            <node concept="1LFfDK" id="4GdRdje$nlZ" role="1urrMF">
              <node concept="3cmrfG" id="4GdRdje$nm6" role="1LF_Uc">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="4GdRdje$o2O" role="1LFl5Q">
                <ref role="3cqZAo" node="4GdRdje$m3p" resolve="result" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3fqX7Q" id="3W99A5svcrG" role="3clFbw">
          <node concept="1LFfDK" id="4GdRdje$mTA" role="3fr31v">
            <node concept="3cmrfG" id="4GdRdje$mVl" role="1LF_Uc">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="37vLTw" id="4GdRdje$m_d" role="1LFl5Q">
              <ref role="3cqZAo" node="4GdRdje$m3p" resolve="result" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="3W99A5ssDHX" role="1YuTPh">
      <property role="TrG5h" value="grid" />
      <ref role="1YaFvo" to="ynv:7M2dKfl0rr2" resolve="Grid" />
    </node>
  </node>
  <node concept="18kY7G" id="4GdRdjeznP9">
    <property role="TrG5h" value="shelf_amount_rule" />
    <node concept="3clFbS" id="4GdRdjeznPa" role="18ibNy">
      <node concept="3cpWs8" id="4GdRdjezC9J" role="3cqZAp">
        <node concept="3cpWsn" id="4GdRdjezC9M" role="3cpWs9">
          <property role="TrG5h" value="edgeAmount" />
          <node concept="10Oyi0" id="4GdRdjezC9H" role="1tU5fm" />
          <node concept="2OqwBi" id="4GdRdjezBHl" role="33vP2m">
            <node concept="2OqwBi" id="4GdRdjezxt4" role="2Oq$k0">
              <node concept="2OqwBi" id="4GdRdjezvDn" role="2Oq$k0">
                <node concept="1PxgMI" id="4GdRdjezvtP" role="2Oq$k0">
                  <node concept="chp4Y" id="4GdRdjezvvw" role="3oSUPX">
                    <ref role="cht4Q" to="ynv:7M2dKfl0rr2" resolve="Grid" />
                  </node>
                  <node concept="2OqwBi" id="4GdRdjezvpQ" role="1m5AlR">
                    <node concept="1YBJjd" id="4GdRdjeztCl" role="2Oq$k0">
                      <ref role="1YBMHb" node="4GdRdjeznPc" resolve="node" />
                    </node>
                    <node concept="1mfA1w" id="4GdRdjezvsq" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="4GdRdjezvNb" role="2OqNvi">
                  <ref role="3TtcxE" to="ynv:7M2dKfl0ybK" resolve="edges" />
                </node>
              </node>
              <node concept="3zZkjj" id="4GdRdjezyLh" role="2OqNvi">
                <node concept="1bVj0M" id="4GdRdjezyLj" role="23t8la">
                  <node concept="3clFbS" id="4GdRdjezyLk" role="1bW5cS">
                    <node concept="3clFbF" id="4GdRdjezySN" role="3cqZAp">
                      <node concept="22lmx$" id="4GdRdjez_94" role="3clFbG">
                        <node concept="3clFbC" id="4GdRdjezB5j" role="3uHU7w">
                          <node concept="2OqwBi" id="4GdRdjezBlB" role="3uHU7w">
                            <node concept="1YBJjd" id="4GdRdjezBdZ" role="2Oq$k0">
                              <ref role="1YBMHb" node="4GdRdjeznPc" resolve="node" />
                            </node>
                            <node concept="3TrcHB" id="4GdRdjezBp_" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4GdRdjezAkF" role="3uHU7B">
                            <node concept="2OqwBi" id="4GdRdjez_ko" role="2Oq$k0">
                              <node concept="37vLTw" id="4GdRdjez_e6" role="2Oq$k0">
                                <ref role="3cqZAo" node="4GdRdjezyLl" resolve="it" />
                              </node>
                              <node concept="3TrEf2" id="4GdRdjez_XZ" role="2OqNvi">
                                <ref role="3Tt5mk" to="ynv:3W99A5ss2wp" resolve="node2" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="4GdRdjezAzb" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbC" id="4GdRdjez$vC" role="3uHU7B">
                          <node concept="2OqwBi" id="4GdRdjezzOS" role="3uHU7B">
                            <node concept="2OqwBi" id="4GdRdjezzda" role="2Oq$k0">
                              <node concept="37vLTw" id="4GdRdjezySM" role="2Oq$k0">
                                <ref role="3cqZAo" node="4GdRdjezyLl" resolve="it" />
                              </node>
                              <node concept="3TrEf2" id="4GdRdjezz$m" role="2OqNvi">
                                <ref role="3Tt5mk" to="ynv:3W99A5ss2wn" resolve="node1" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="4GdRdjez$0Z" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4GdRdjez$VB" role="3uHU7w">
                            <node concept="1YBJjd" id="4GdRdjez$On" role="2Oq$k0">
                              <ref role="1YBMHb" node="4GdRdjeznPc" resolve="node" />
                            </node>
                            <node concept="3TrcHB" id="4GdRdjez_48" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="4GdRdjezyLl" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="4GdRdjezyLm" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="34oBXx" id="4GdRdjezC1e" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="4GdRdjezo6I" role="3cqZAp">
        <node concept="3eOSWO" id="4GdRdjezRCu" role="3clFbw">
          <node concept="2OqwBi" id="4GdRdjezpUN" role="3uHU7B">
            <node concept="2OqwBi" id="4GdRdjezo8U" role="2Oq$k0">
              <node concept="1YBJjd" id="4GdRdjezo6U" role="2Oq$k0">
                <ref role="1YBMHb" node="4GdRdjeznPc" resolve="node" />
              </node>
              <node concept="3Tsc0h" id="4GdRdjezoi_" role="2OqNvi">
                <ref role="3TtcxE" to="ynv:3W99A5ss4ms" resolve="shelfs" />
              </node>
            </node>
            <node concept="34oBXx" id="4GdRdjezr$E" role="2OqNvi" />
          </node>
          <node concept="3cpWsd" id="4GdRdjeztBc" role="3uHU7w">
            <node concept="3cmrfG" id="4GdRdjeztx0" role="3uHU7B">
              <property role="3cmrfH" value="4" />
            </node>
            <node concept="37vLTw" id="4GdRdjezCKn" role="3uHU7w">
              <ref role="3cqZAo" node="4GdRdjezC9M" resolve="edgeAmount" />
            </node>
          </node>
        </node>
        <node concept="3clFbS" id="4GdRdjezo6K" role="3clFbx">
          <node concept="2MkqsV" id="4GdRdjezC6l" role="3cqZAp">
            <node concept="3cpWs3" id="4GdRdjezFud" role="2MkJ7o">
              <node concept="Xl_RD" id="4GdRdjezFvt" role="3uHU7w">
                <property role="Xl_RC" value=" shelves" />
              </node>
              <node concept="3cpWs3" id="4GdRdjezEIT" role="3uHU7B">
                <node concept="3cpWs3" id="4GdRdjezEos" role="3uHU7B">
                  <node concept="3cpWs3" id="4GdRdjezDuZ" role="3uHU7B">
                    <node concept="Xl_RD" id="4GdRdjezDcK" role="3uHU7B">
                      <property role="Xl_RC" value="Due to this node's " />
                    </node>
                    <node concept="37vLTw" id="4GdRdjezDvh" role="3uHU7w">
                      <ref role="3cqZAo" node="4GdRdjezC9M" resolve="edgeAmount" />
                    </node>
                  </node>
                  <node concept="Xl_RD" id="4GdRdjezEp4" role="3uHU7w">
                    <property role="Xl_RC" value=" edges it can only have " />
                  </node>
                </node>
                <node concept="1eOMI4" id="4GdRdjezFxW" role="3uHU7w">
                  <node concept="3cpWsd" id="4GdRdjezEKt" role="1eOMHV">
                    <node concept="3cmrfG" id="4GdRdjezEJJ" role="3uHU7B">
                      <property role="3cmrfH" value="4" />
                    </node>
                    <node concept="37vLTw" id="4GdRdjezELx" role="3uHU7w">
                      <ref role="3cqZAo" node="4GdRdjezC9M" resolve="edgeAmount" />
                    </node>
                  </node>
                </node>
              </node>
            </node>
            <node concept="1YBJjd" id="4GdRdjezFED" role="1urrMF">
              <ref role="1YBMHb" node="4GdRdjeznPc" resolve="node" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4GdRdjeznPc" role="1YuTPh">
      <property role="TrG5h" value="node" />
      <ref role="1YaFvo" to="ynv:7M2dKfl0rrg" resolve="Node" />
    </node>
  </node>
  <node concept="18kY7G" id="4GdRdjezWeD">
    <property role="TrG5h" value="maximum_node_edges" />
    <node concept="3clFbS" id="4GdRdjezWeE" role="18ibNy">
      <node concept="3cpWs8" id="4GdRdjezXpP" role="3cqZAp">
        <node concept="3cpWsn" id="4GdRdjezXpS" role="3cpWs9">
          <property role="TrG5h" value="edgeAmount" />
          <node concept="10Oyi0" id="4GdRdjezXpO" role="1tU5fm" />
          <node concept="2OqwBi" id="4GdRdjezXqb" role="33vP2m">
            <node concept="2OqwBi" id="4GdRdjezXqc" role="2Oq$k0">
              <node concept="2OqwBi" id="4GdRdjezXqd" role="2Oq$k0">
                <node concept="1PxgMI" id="4GdRdjezXqe" role="2Oq$k0">
                  <node concept="chp4Y" id="4GdRdjezXqf" role="3oSUPX">
                    <ref role="cht4Q" to="ynv:7M2dKfl0rr2" resolve="Grid" />
                  </node>
                  <node concept="2OqwBi" id="4GdRdjezXqg" role="1m5AlR">
                    <node concept="1YBJjd" id="4GdRdjezXqh" role="2Oq$k0">
                      <ref role="1YBMHb" node="4GdRdjezWeG" resolve="node" />
                    </node>
                    <node concept="1mfA1w" id="4GdRdjezXqi" role="2OqNvi" />
                  </node>
                </node>
                <node concept="3Tsc0h" id="4GdRdjezXqj" role="2OqNvi">
                  <ref role="3TtcxE" to="ynv:7M2dKfl0ybK" resolve="edges" />
                </node>
              </node>
              <node concept="3zZkjj" id="4GdRdjezXqk" role="2OqNvi">
                <node concept="1bVj0M" id="4GdRdjezXql" role="23t8la">
                  <node concept="3clFbS" id="4GdRdjezXqm" role="1bW5cS">
                    <node concept="3clFbF" id="4GdRdjezXqn" role="3cqZAp">
                      <node concept="22lmx$" id="4GdRdjezXqo" role="3clFbG">
                        <node concept="3clFbC" id="4GdRdjezXqp" role="3uHU7w">
                          <node concept="2OqwBi" id="4GdRdjezXqq" role="3uHU7w">
                            <node concept="1YBJjd" id="4GdRdjezXqr" role="2Oq$k0">
                              <ref role="1YBMHb" node="4GdRdjezWeG" resolve="node" />
                            </node>
                            <node concept="3TrcHB" id="4GdRdjezXqs" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4GdRdjezXqt" role="3uHU7B">
                            <node concept="2OqwBi" id="4GdRdjezXqu" role="2Oq$k0">
                              <node concept="37vLTw" id="4GdRdjezXqv" role="2Oq$k0">
                                <ref role="3cqZAo" node="4GdRdjezXqF" resolve="it" />
                              </node>
                              <node concept="3TrEf2" id="4GdRdjezXqw" role="2OqNvi">
                                <ref role="3Tt5mk" to="ynv:3W99A5ss2wp" resolve="node2" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="4GdRdjezXqx" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                        <node concept="3clFbC" id="4GdRdjezXqy" role="3uHU7B">
                          <node concept="2OqwBi" id="4GdRdjezXqz" role="3uHU7B">
                            <node concept="2OqwBi" id="4GdRdjezXq$" role="2Oq$k0">
                              <node concept="37vLTw" id="4GdRdjezXq_" role="2Oq$k0">
                                <ref role="3cqZAo" node="4GdRdjezXqF" resolve="it" />
                              </node>
                              <node concept="3TrEf2" id="4GdRdjezXqA" role="2OqNvi">
                                <ref role="3Tt5mk" to="ynv:3W99A5ss2wn" resolve="node1" />
                              </node>
                            </node>
                            <node concept="3TrcHB" id="4GdRdjezXqB" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                          <node concept="2OqwBi" id="4GdRdjezXqC" role="3uHU7w">
                            <node concept="1YBJjd" id="4GdRdjezXqD" role="2Oq$k0">
                              <ref role="1YBMHb" node="4GdRdjezWeG" resolve="node" />
                            </node>
                            <node concept="3TrcHB" id="4GdRdjezXqE" role="2OqNvi">
                              <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                            </node>
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                  <node concept="Rh6nW" id="4GdRdjezXqF" role="1bW2Oz">
                    <property role="TrG5h" value="it" />
                    <node concept="2jxLKc" id="4GdRdjezXqG" role="1tU5fm" />
                  </node>
                </node>
              </node>
            </node>
            <node concept="34oBXx" id="4GdRdjezXqH" role="2OqNvi" />
          </node>
        </node>
      </node>
      <node concept="3clFbH" id="4GdRdjezXZG" role="3cqZAp" />
      <node concept="3clFbJ" id="4GdRdjezY1e" role="3cqZAp">
        <node concept="3clFbS" id="4GdRdjezY1g" role="3clFbx">
          <node concept="2MkqsV" id="4GdRdjezYRF" role="3cqZAp">
            <node concept="Xl_RD" id="4GdRdjezYRU" role="2MkJ7o">
              <property role="Xl_RC" value="This node contains more than 4 edges" />
            </node>
            <node concept="1YBJjd" id="4GdRdje$1ZJ" role="1urrMF">
              <ref role="1YBMHb" node="4GdRdjezWeG" resolve="node" />
            </node>
          </node>
        </node>
        <node concept="3eOSWO" id="4GdRdjezYQZ" role="3clFbw">
          <node concept="3cmrfG" id="4GdRdjezYRo" role="3uHU7w">
            <property role="3cmrfH" value="4" />
          </node>
          <node concept="37vLTw" id="4GdRdjezY2c" role="3uHU7B">
            <ref role="3cqZAo" node="4GdRdjezXpS" resolve="edgeAmount" />
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="4GdRdjezWeG" role="1YuTPh">
      <property role="TrG5h" value="node" />
      <ref role="1YaFvo" to="ynv:7M2dKfl0rrg" resolve="Node" />
    </node>
  </node>
  <node concept="18kY7G" id="6f9o7wY4BQ6">
    <property role="TrG5h" value="duplicate_node_location" />
    <node concept="3clFbS" id="6f9o7wY4BQ7" role="18ibNy">
      <node concept="3clFbJ" id="6f9o7wY4C2S" role="3cqZAp">
        <node concept="3clFbS" id="6f9o7wY4C2T" role="3clFbx">
          <node concept="2MkqsV" id="6f9o7wY4C2U" role="3cqZAp">
            <node concept="3cpWs3" id="6f9o7wY4C2V" role="2MkJ7o">
              <node concept="2OqwBi" id="6f9o7wY4C2W" role="3uHU7w">
                <node concept="1YBJjd" id="6f9o7wY4C2X" role="2Oq$k0">
                  <ref role="1YBMHb" node="6f9o7wY4BQ9" resolve="node" />
                </node>
                <node concept="3TrcHB" id="6f9o7wY4C2Y" role="2OqNvi">
                  <ref role="3TsBF5" to="tpck:h0TrG11" resolve="name" />
                </node>
              </node>
              <node concept="Xl_RD" id="6f9o7wY4C2Z" role="3uHU7B">
                <property role="Xl_RC" value="This (x,y) location already exists: " />
              </node>
            </node>
            <node concept="1YBJjd" id="6f9o7wY4C30" role="1urrMF">
              <ref role="1YBMHb" node="6f9o7wY4BQ9" resolve="node" />
            </node>
          </node>
        </node>
        <node concept="2OqwBi" id="6f9o7wY4C31" role="3clFbw">
          <node concept="2OqwBi" id="6f9o7wY4C32" role="2Oq$k0">
            <node concept="1PxgMI" id="6f9o7wY4C33" role="2Oq$k0">
              <node concept="chp4Y" id="6f9o7wY4C34" role="3oSUPX">
                <ref role="cht4Q" to="ynv:7M2dKfl0rr2" resolve="Grid" />
              </node>
              <node concept="2OqwBi" id="6f9o7wY4C35" role="1m5AlR">
                <node concept="1YBJjd" id="6f9o7wY4C36" role="2Oq$k0">
                  <ref role="1YBMHb" node="6f9o7wY4BQ9" resolve="node" />
                </node>
                <node concept="1mfA1w" id="6f9o7wY4C37" role="2OqNvi" />
              </node>
            </node>
            <node concept="3Tsc0h" id="6f9o7wY4C38" role="2OqNvi">
              <ref role="3TtcxE" to="ynv:7M2dKfl0ybM" resolve="nodes" />
            </node>
          </node>
          <node concept="2HwmR7" id="6f9o7wY4C39" role="2OqNvi">
            <node concept="1bVj0M" id="6f9o7wY4C3a" role="23t8la">
              <node concept="3clFbS" id="6f9o7wY4C3b" role="1bW5cS">
                <node concept="3clFbF" id="6f9o7wY4C3c" role="3cqZAp">
                  <node concept="1Wc70l" id="6f9o7wY4C3d" role="3clFbG">
                    <node concept="17QLQc" id="6f9o7wY4C3e" role="3uHU7w">
                      <node concept="1YBJjd" id="6f9o7wY4C3f" role="3uHU7w">
                        <ref role="1YBMHb" node="6f9o7wY4BQ9" resolve="node" />
                      </node>
                      <node concept="37vLTw" id="6f9o7wY4C3g" role="3uHU7B">
                        <ref role="3cqZAo" node="6f9o7wY4C3o" resolve="it" />
                      </node>
                    </node>
                    <node concept="1Wc70l" id="6f9o7wY4DSw" role="3uHU7B">
                      <node concept="17R0WA" id="6f9o7wY4EzY" role="3uHU7w">
                        <node concept="2OqwBi" id="6f9o7wY4Far" role="3uHU7w">
                          <node concept="1YBJjd" id="6f9o7wY4EFE" role="2Oq$k0">
                            <ref role="1YBMHb" node="6f9o7wY4BQ9" resolve="node" />
                          </node>
                          <node concept="3TrcHB" id="6f9o7wY4FuE" role="2OqNvi">
                            <ref role="3TsBF5" to="ynv:6f9o7wY4y8L" resolve="y" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="6f9o7wY4E9K" role="3uHU7B">
                          <node concept="37vLTw" id="6f9o7wY4DWG" role="2Oq$k0">
                            <ref role="3cqZAo" node="6f9o7wY4C3o" resolve="it" />
                          </node>
                          <node concept="3TrcHB" id="6f9o7wY4EeV" role="2OqNvi">
                            <ref role="3TsBF5" to="ynv:6f9o7wY4y8L" resolve="y" />
                          </node>
                        </node>
                      </node>
                      <node concept="17R0WA" id="6f9o7wY4C3h" role="3uHU7B">
                        <node concept="2OqwBi" id="6f9o7wY4C3i" role="3uHU7B">
                          <node concept="37vLTw" id="6f9o7wY4C3j" role="2Oq$k0">
                            <ref role="3cqZAo" node="6f9o7wY4C3o" resolve="it" />
                          </node>
                          <node concept="3TrcHB" id="6f9o7wY4CGI" role="2OqNvi">
                            <ref role="3TsBF5" to="ynv:6f9o7wY4y8I" resolve="x" />
                          </node>
                        </node>
                        <node concept="2OqwBi" id="6f9o7wY4C3l" role="3uHU7w">
                          <node concept="1YBJjd" id="6f9o7wY4C3m" role="2Oq$k0">
                            <ref role="1YBMHb" node="6f9o7wY4BQ9" resolve="node" />
                          </node>
                          <node concept="3TrcHB" id="6f9o7wY4D1n" role="2OqNvi">
                            <ref role="3TsBF5" to="ynv:6f9o7wY4y8I" resolve="x" />
                          </node>
                        </node>
                      </node>
                    </node>
                  </node>
                </node>
              </node>
              <node concept="Rh6nW" id="6f9o7wY4C3o" role="1bW2Oz">
                <property role="TrG5h" value="it" />
                <node concept="2jxLKc" id="6f9o7wY4C3p" role="1tU5fm" />
              </node>
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6f9o7wY4BQ9" role="1YuTPh">
      <property role="TrG5h" value="node" />
      <ref role="1YaFvo" to="ynv:7M2dKfl0rrg" resolve="Node" />
    </node>
  </node>
  <node concept="18kY7G" id="6f9o7wY4W$_">
    <property role="TrG5h" value="valid_edge" />
    <node concept="3clFbS" id="6f9o7wY4W$A" role="18ibNy">
      <node concept="3cpWs8" id="6f9o7wY4YiO" role="3cqZAp">
        <node concept="3cpWsn" id="6f9o7wY4YiR" role="3cpWs9">
          <property role="TrG5h" value="result" />
          <node concept="1LlUBW" id="6f9o7wY4YiS" role="1tU5fm">
            <node concept="10P_77" id="6f9o7wY5vuU" role="1Lm7xW" />
            <node concept="3Tqbb2" id="6f9o7wY4YiU" role="1Lm7xW">
              <ref role="ehGHo" to="ynv:7M2dKfl0rra" resolve="Edge" />
            </node>
          </node>
          <node concept="2OqwBi" id="6f9o7wY4YiV" role="33vP2m">
            <node concept="1YBJjd" id="6f9o7wY4YiW" role="2Oq$k0">
              <ref role="1YBMHb" node="6f9o7wY4W$C" resolve="grid" />
            </node>
            <node concept="2qgKlT" id="6f9o7wY4Yuk" role="2OqNvi">
              <ref role="37wK5l" to="tdwz:6f9o7wY4Azq" resolve="checkEdges" />
            </node>
          </node>
        </node>
      </node>
      <node concept="3clFbJ" id="6f9o7wY4Ywz" role="3cqZAp">
        <node concept="3clFbS" id="6f9o7wY4Yw_" role="3clFbx">
          <node concept="2MkqsV" id="6f9o7wY4YUY" role="3cqZAp">
            <node concept="Xl_RD" id="6f9o7wY4YVd" role="2MkJ7o">
              <property role="Xl_RC" value="Nodes are not next to each other" />
            </node>
            <node concept="1LFfDK" id="6f9o7wY4ZjL" role="1urrMF">
              <node concept="3cmrfG" id="6f9o7wY4ZjS" role="1LF_Uc">
                <property role="3cmrfH" value="1" />
              </node>
              <node concept="37vLTw" id="6f9o7wY4YW2" role="1LFl5Q">
                <ref role="3cqZAo" node="6f9o7wY4YiR" resolve="result" />
              </node>
            </node>
          </node>
        </node>
        <node concept="3fqX7Q" id="6f9o7wY4YUl" role="3clFbw">
          <node concept="1LFfDK" id="6f9o7wY4YUn" role="3fr31v">
            <node concept="3cmrfG" id="6f9o7wY4YUo" role="1LF_Uc">
              <property role="3cmrfH" value="0" />
            </node>
            <node concept="37vLTw" id="6f9o7wY4YUp" role="1LFl5Q">
              <ref role="3cqZAo" node="6f9o7wY4YiR" resolve="result" />
            </node>
          </node>
        </node>
      </node>
    </node>
    <node concept="1YaCAy" id="6f9o7wY4W$C" role="1YuTPh">
      <property role="TrG5h" value="grid" />
      <ref role="1YaFvo" to="ynv:7M2dKfl0rr2" resolve="Grid" />
    </node>
  </node>
</model>

