<?xml version="1.0" encoding="UTF-8"?>
<solution name="FacilityProduct.runtime" uuid="99354c6d-4c3f-4e68-8614-bbed88e862f4" moduleVersion="0" compileInMPS="true">
  <models>
    <modelRoot contentPath="${module}" type="default">
      <sourceRoot location="models" />
    </modelRoot>
  </models>
  <facets>
    <facet type="java">
      <classes generated="true" path="${module}/classes_gen" />
    </facet>
  </facets>
  <sourcePath />
  <languageVersions />
  <dependencyVersions>
    <module reference="99354c6d-4c3f-4e68-8614-bbed88e862f4(FacilityProduct.runtime)" version="0" />
  </dependencyVersions>
</solution>

